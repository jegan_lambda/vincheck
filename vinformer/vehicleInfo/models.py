from django.db import models



class VehicleReport(models.Model):
    vin_number = models.CharField(db_column='VIN NUMBER', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    registration_number = models.CharField(db_column='REGISTRATION NUMBER', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    country_of_origin = models.CharField(db_column='Country of Origin', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    make = models.CharField(db_column='Make', max_length=100, blank=True, null=True)  # Field name made lowercase.
    model = models.CharField(db_column='Model', max_length=100, blank=True, null=True)  # Field name made lowercase.
    variant = models.CharField(db_column='Variant', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    model_year = models.CharField(db_column='Model Year', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ex_showroom_price = models.CharField(db_column='Ex-showroom price', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    manufacture_month_year = models.CharField(db_column='Manufacture Month & Year', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    registration_month_year = models.CharField(db_column='Registration Month & Year', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    transmission_type = models.CharField(db_column='Transmission Type', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    drivetrain = models.CharField(db_column='Drivetrain', max_length=100, blank=True, null=True)  # Field name made lowercase.
    no_of_gears = models.CharField(db_column='No of gears', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    valuation_date = models.CharField(db_column='Valuation Date', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    current_market_value_good = models.CharField(db_column='Current Market Value (Good)', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    current_market_value_average = models.CharField(db_column='Current Market Value (Average)', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    current_market_value_bad = models.CharField(db_column='Current Market Value (Bad)', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    horse_power_bhp = models.CharField(db_column='Horse Power (bhp)', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    cylinders = models.CharField(db_column='Cylinders', max_length=100, blank=True, null=True)  # Field name made lowercase.
    torque = models.CharField(db_column='Torque', max_length=100, blank=True, null=True)  # Field name made lowercase.
    engine_capacity = models.CharField(db_column='Engine Capacity', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fuel_type = models.CharField(db_column='Fuel Type', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    alternate_fuel_type = models.CharField(db_column='Alternate fuel type', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mileage_kmpl = models.CharField(db_column='Mileage (kmpl)', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    fuel_tank_capacity_litres = models.CharField(db_column='Fuel tank capacity (litres)', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    body_type = models.CharField(db_column='Body Type', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    length_mm = models.CharField(db_column='Length (mm)', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    width_mm = models.CharField(db_column='Width (mm)', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    height_mm = models.CharField(db_column='Height (mm)', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    wheelbase_mm = models.CharField(db_column='Wheelbase (mm)', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    no_of_doors = models.CharField(db_column='No of doors', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    roof = models.CharField(db_column='Roof', max_length=100, blank=True, null=True)  # Field name made lowercase.
    seating_capacity = models.CharField(db_column='Seating capacity', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tyres_front = models.CharField(db_column='Tyres front', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tyres_rear = models.CharField(db_column='Tyres rear', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    top_speed = models.CharField(db_column='Top Speed (Km/h)', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    acceleration = models.CharField(db_column='Acceleration 0-100 Km/h (sec)', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    trunk_capacity = models.CharField(db_column='Trunk Capacity (liters)', max_length=100, blank=True,null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.

    class Meta:
        managed = True
        db_table = 'vehicle_report'
