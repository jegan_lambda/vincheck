from django.apps import AppConfig


class VehicleinfoConfig(AppConfig):
    name = 'vehicleInfo'
