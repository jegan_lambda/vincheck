from rest_framework.viewsets import ModelViewSet,ReadOnlyModelViewSet
import django_filters.rest_framework
from rest_framework import viewsets
from .serializers import *
from rest_framework.response import Response
from rest_framework import filters,generics
from  datetime import datetime,date,timedelta
import json
import hashlib
import requests
import xml.etree.ElementTree as ET
import configparser
config = configparser.ConfigParser()
config.read('config.ini')


def getMKT(manufactyrs,exs,dep):
    if(manufactyrs and exs):
        manufactyrs = int(manufactyrs)
        exs = int(exs)
        syd = 5 if manufactyrs > (date.today().year - 5) else 10

        smd = syd / 12
        month = 1
        manufactdate = datetime(manufactyrs, month, 1)
        current = datetime.today()
        prev = current - timedelta(days=current.day)

        age = (prev.year - manufactdate.year) * 12 + (prev.month - manufactdate.month)
        sdp = age * smd
        total_depreciation = sdp
        final_depreciation = exs * (total_depreciation / 100)
        fdp = exs - final_depreciation


        adjust = int(dep)
        vmv_depreciation = fdp * (adjust / 100)

        vmv = fdp + vmv_depreciation

        dicts = {"Manufacture Date": manufactdate.date(),
                 "Ex-showroom Price": exs,
                 "Vehicle Market Value": round(vmv, 2)}
        return dicts
    else:
        return {}


def getType(rt):
	for r in rt.getchildren():
		if (r.tag == '{http://regcheck.org.uk}vehicleJson'):
			return r.text
		else:
			return getType(r)




class infoview(ModelViewSet):
    queryset = VehicleReport.objects.all()
    serializer_class = vehicledetailserializers
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = {'vin_number': ["exact"],'registration_number': ["exact"], 'make': ["icontains"], 'model': ["icontains"], 'variant': ["icontains"],
                            'model_year': ["exact"], }

    xml = """<?xml version="1.0" encoding="utf-8"?>
           <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
             <soap:Body>
               <CheckUAE xmlns="http://regcheck.org.uk">
                 <RegistrationNumber>{}</RegistrationNumber>
                 <username>{}</username>
               </CheckUAE>
             </soap:Body>
           </soap:Envelope>"""
    specheaders = {'Content-Type': 'text/xml'}

    def list(self, request, *args, **kwargs):
        try:
            vin = self.request.query_params.get('vin_number')
            reg_no = self.request.query_params.get('registration_number')
            # dep = self.request.query_params.get('dep')
            vinApiEndpoint = config['VINFORMER']['VIN_API_ENDPOINT']
            carApiEndpoint = config['VINFORMER']['CAR_API_ENDPOINT']
            username = config['VINFORMER']['USERNAME']
            partnerId = config['VINFORMER']['PARTNER_ID']
            secretkey = config['VINFORMER']['SECRETKEY']
            dep = config['VINFORMER']['depreciation']
            message = "No Data Available"
            Jsons = {}
            if dep:
                dep = int(dep)
            else:
                dep = 0
            if reg_no:
                try:
                    e = requests.post(carApiEndpoint, data=self.xml.format(reg_no,username), headers=self.specheaders,timeout=20)
                except requests.exceptions.RequestException as e:
                    raise ConnectionError(str(e))

                if e.status_code == 200:
                    xmls = ET.fromstring(e.text)
                    redpJso = getType(xmls)
                    if redpJso:
                        mk = json.loads(redpJso.replace("\n", ""))
                        cMake = mk.get('CarMake') # CurrentTextValue
                        cModel = mk.get('CarModel')
                        if cMake and cModel:
                            cMake,cModel,logo = str(cMake.get('CurrentTextValue')),str(cModel.get('CurrentTextValue')),str(mk.get('ImageUrl'))
                            Query = self.queryset.filter(make__icontains=cMake.strip(), model__icontains=cModel.strip())
                            data = Query.values()[0] if Query.values() else {}
                            mkt = {"Price": getMKT(data.get('model_year'), data.get('ex_showroom_price'), dep)}
                            Data = {"Basic": data}
                            api = {"apiResp": {"logo":logo}}
                            return Response({'count':0,'next':None,'prev':None,'results':{**Data, **mkt, **api}})
                else:
                    Data = {"Basic": {}}
                    api = {"apiResp": {}}
                    mkt = {"Price": {}}
                    return Response({'count':0,'next':None,'prev':None,"Message": "No Data Available",'results':{**Data, **mkt, **api} })

            if vin:
                apikey = getMD5(vin, secretkey)
                try:
                    Jsons = requests.get(vinApiEndpoint, params={"id": partnerId, "vin": vin, "key": apikey},timeout=15)
                except requests.exceptions.RequestException as e:
                    raise ConnectionError(str(e))
                if Jsons and Jsons.json().get('status') == 'OK':
                    d = getRequired(Jsons.json())
                else:
                    d = {}
            else:
                d = {}

            queryset = self.filter_queryset(self.get_queryset())

            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                if serializer.data:
                    data = serializer.data[0]
                    mkt = {"Price": getMKT(data.get('model_year'), data.get('ex_showroom_price'), dep)}

                else:
                    if Jsons and 'data' in Jsons.json():
                        Jso = Jsons.json()['data']
                        brand = str(Jso.get('brand')).strip()
                        model = str(Jso.get('model')).strip()
                        year = str(Jso.get('year')).strip()
                        Query = self.queryset.filter(make__icontains = brand,model__icontains = model,model_year = year)
                        data = Query.values()[0] if Query.values() else {}
                        mkt = {"Price": getMKT(data.get('model_year'), data.get('ex_showroom_price'), dep)}
                    else:
                        data = {}
                        mkt = {"Price":{}}
                Data = {"Basic":data}
                api = {"apiResp":d}
                return self.get_paginated_response({**Data,**mkt,**api})

            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)
        except ConnectionError as e:
            print(str(e))
            Data = {"Basic": {}}
            api = {"apiResp": {}}
            mkt = {"Price": {}}
            return Response({'count': 0, 'next': None, 'prev': None, "Message": "Connection Timeout",
                             'results': {**Data, **mkt, **api}})

        except Exception as e:
            print(str(e))
            Data = {"Basic": {}}
            api = {"apiResp": {}}
            mkt = {"Price": {}}
            return Response({'count': 0, 'next': None, 'prev': None, "Message": "No Data Available",
                             'results': {**Data, **mkt, **api}})




def getMD5(vin,secretkey):
    #convert into hexadecimal equivalent string
    key = vin + secretkey
    result = hashlib.md5(key.encode())
    return result.hexdigest()

def getRequired(Jsons):
    dicts ={}
    for d in Jsons['report']:
        if d[2]:
            dicts[d[1]] = list(map(lambda x:{x[1]:x[-1]},d[2]))
    return {**Jsons["data"],**{"logo":Jsons["logo"]},**dicts}






